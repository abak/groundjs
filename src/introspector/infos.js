/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * base: A GIBaseInfo instance
 * info: A Gjs_Info instance
 * path: A JS expression (e.g. 'GObject.SignalFlags.RUN_FIRST')
 */

import { Gir, GObject } from './gi.js';

Gir.BaseInfo.prototype.getPath = function() {
    return `${this.get_namespace()}.${this.get_name()}`;
};

// Serve as both a base class and a factory for the subclasses.
export const Info = GObject.registerClass({
    Properties: {
        'base': GObject.ParamSpec.boxed(
            'base', "Base", "The underlying base info structure",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, Gir.BaseInfo.$gtype
        ),
        'string': GObject.ParamSpec.string(
            'string', "String", "The string to display",
            GObject.ParamFlags.READABLE, null
        ),
    },
}, class Info extends GObject.Object {
    static newForBase(base) {
        switch (base.get_type()) {
            case Gir.InfoType.FUNCTION:
                return new FunctionInfo({ base });
            case Gir.InfoType.CALLBACK:
                return new CallbackInfo({ base });
            case Gir.InfoType.SIGNAL:
                return new SignalInfo({ base });
            case Gir.InfoType.VFUNC:
                return new VFuncInfo({ base });
            case Gir.InfoType.ENUM:
            case Gir.InfoType.FLAGS:
                return new EnumInfo({ base });
            case Gir.InfoType.STRUCT:
                return new StructInfo({ base });
            case Gir.InfoType.UNION:
                return new UnionInfo({ base });
            case Gir.InfoType.OBJECT:
                return new ObjectInfo({ base });
            case Gir.InfoType.INTERFACE:
                return new InterfaceInfo({ base });
            case Gir.InfoType.ARG:
                return new ArgInfo({ base });
            case Gir.InfoType.CONSTANT:
                return new ConstantInfo({ base });
            case Gir.InfoType.FIELD:
                return new FieldInfo({ base });
            case Gir.InfoType.PROPERTY:
                return new PropertyInfo({ base });
            case Gir.InfoType.TYPE:
                return new TypeInfo({ base });
            case Gir.InfoType.VALUE:
                return new ValueInfo({ base });
            default:
                return new this({ base });
        }
    }

    get string() {
        return `${this.base.get_name()} (${Gir.info_type_to_string(this.base.get_type())})`;
    }

    has(name) {
        return !!this[name == 'property' ? `get_n_properties` : `get_n_${name}s`]();
    }

    getBases(name) {
        return Array.from(
            { length: this[name == 'property' ? `get_n_properties` : `get_n_${name}s`]() },
            (e_, i) => this[`get_${name}`](i)
        );
    }

    getInfo(name) {
        let base = this[`get_${name}`]();
        if (!base)
            return null;

        return Info.newForBase(this[`get_${name}`]());
    }

    getInfos(name) {
        return this.getBases(name).map(base => Info.newForBase(base));
    }
});

// Append the Gir info functions to the new info classes.
// For example: "Gir.object_info_get_method(baseInfo, n)" -> "objectInfo.get_method(n)".
const registerInfoClass = function(metaInfo, klass) {
    Object.keys(Gir).forEach(key => {
        if (key.startsWith(metaInfo.Basename))
            klass.prototype[key.slice(metaInfo.Basename.length + 1)] = function(...args) {
                return Gir[key].call(null, this.base, ...args);
            };
    });
    delete metaInfo.Basename;

    return GObject.registerClass(metaInfo, klass);
};

export const CallableInfo = registerInfoClass({
    Basename: 'callable_info',
}, class CallableInfo extends Info {
    get _args() {
        return this.__args ?? (this.__args = this.getInfos('arg'));
    }

    get inArgs() {
        let skippedIndexes = [];
        this._args.forEach(info => skippedIndexes.push(...info.skippedIndexes));

        return this._args.filter((info, index) => {
            return !skippedIndexes.includes(index) && info.get_direction() != Gir.Direction.OUT;
        });
    }

    get outArgs() {
        return this._args.filter(info => info.get_direction() == Gir.Direction.OUT);
    }

    get returnString() {
        let info = this.getInfo('return_type');
        if (this.skip_return() || info.get_tag() == Gir.TypeTag.VOID)
            return null;
        return `${info.string}${this.may_return_null() ? " or null" : ""}`;
    }
});

export const FunctionInfo = registerInfoClass({
    Basename: 'function_info',
}, class FunctionInfo extends CallableInfo {
    get flagsPath() {
        return Object.entries(Gir.FunctionInfoFlags)
            .filter(([key, value]) => (typeof value) == 'number' && (this.get_flags() & value))
            .map(([key, value]) => `Gir.FunctionInfoFlags.${key}`)
            .join(" | ");
    }
});

export const CallbackInfo = registerInfoClass({
    Basename: 'callback_info',
}, class CallbackInfo extends CallableInfo {
});

export const SignalInfo = registerInfoClass({
    Basename: 'signal_info',
}, class SignalInfo extends CallableInfo {
    get flagsPath() {
        return Object.entries(GObject.SignalFlags)
            .filter(([key, value]) => (typeof value) == 'number' && (this.get_flags() & value))
            .map(([key, value]) => `GObject.SignalFlags.${key}`)
            .join(" | ");
    }

    get hasReturn() {
        return this.getInfo('return_type').get_tag() != Gir.TypeTag.VOID;
    }
});

export const VFuncInfo = registerInfoClass({
    Basename: 'vfunc_info',
}, class VFuncInfo extends CallableInfo {
});

export const RegisteredTypeInfo = registerInfoClass({
    Basename: 'registered_type_info',
}, class RegisteredTypeInfo extends Info {
});

export const EnumInfo = registerInfoClass({
    Basename: 'enum_info',
}, class EnumInfo extends RegisteredTypeInfo {
});

export const StructInfo = registerInfoClass({
    Basename: 'struct_info',
}, class StructInfo extends RegisteredTypeInfo {
});

export const UnionInfo = registerInfoClass({
    Basename: 'union_info',
}, class UnionInfo extends RegisteredTypeInfo {
});

export const ObjectInfo = registerInfoClass({
    Basename: 'object_info',
}, class ObjectInfo extends RegisteredTypeInfo {
    get allFunctions() {
        let [statics, instances, virtuals] = [[], [], [], []];

        this.getInfos('method').forEach(info => {
            if (!(info.get_flags() & Gir.FunctionInfoFlags.IS_METHOD))
                statics.push(info);
            else
                instances.push(info);
        });

        virtuals.push(...this.getInfos('vfunc'));
        statics.push(...(this.getInfo('class_struct')?.getInfos('method') ?? []));
        statics.forEach(info => (info.isStatic = true));

        return [statics, instances, virtuals];
    }
});

export const InterfaceInfo = registerInfoClass({
    Basename: 'interface_info',
}, class InterfaceInfo extends RegisteredTypeInfo {
    get allFunctions() {
        let [statics, instances, virtuals] = [[], [], [], []];

        this.getInfos('method').forEach(info => {
            if (!(info.get_flags() & Gir.FunctionInfoFlags.IS_METHOD))
                statics.push(info);
            else
                instances.push(info);
        });

        virtuals.push(...this.getInfos('vfunc'));
        statics.push(...(this.getInfo('iface_struct')?.getInfos('method') ?? []));
        statics.forEach(info => (info.isStatic = true));

        return [statics, instances, virtuals];
    }
});

export const ArgInfo = registerInfoClass({
    Basename: 'arg_info',
}, class ArgInfo extends Info {
    get _type() {
        return this.__type ?? (this.__type = this.getInfo('type'));
    }

    get skippedIndexes() {
        let skippedIndexes = [];

        if (this.get_closure() != -1)
            skippedIndexes.push(this.get_closure());
        if (this.get_destroy() != -1)
            skippedIndexes.push(this.get_destroy());
        if (this._type.get_array_length() != -1)
            skippedIndexes.push(this._type.get_array_length());

        return skippedIndexes;
    }

    get string() {
        let string = this._type.string;
        if (this.may_be_null())
            string += " or null";
        if (this.is_skip())
            string += " (skip)";
        if (this.is_return_value())
            string += " (return value)";

        return string;
    }

    get typePath() {
        return this._type.path;
    }
});

export const ConstantInfo = registerInfoClass({
    Basename: 'constant_info',
}, class ConstantInfo extends Info {
    get value() {
        try {
            return imports.gi[this.base.get_namespace()][this.base.get_name()];
        } catch(e) {
            // FIXME: see GstBufferCopyFlags.
            return undefined;
        }
    }

    get isString() {
        return this.getInfo('type').string.includes('String');
    }
});

export const FieldInfo = registerInfoClass({
    Basename: 'field_info',
}, class FieldInfo extends Info {
    get readable() {
        return this.get_flags() & Gir.FieldInfoFlags.READABLE;
    }

    get writable() {
        return this.get_flags() & Gir.FieldInfoFlags.WRITABLE;
    }

    get string() {
        return this.getInfo('type').string;
    }
});

export const PropertyInfo = registerInfoClass({
    Basename: 'property_info',
}, class PropertyInfo extends Info {
    get debug() {
        return this.getInfo('type').debug;
    }

    get _flagsPath() {
        return Object.entries(GObject.ParamFlags)
            .filter(([key, value]) => (typeof value) == 'number' && (this.get_flags() & value))
            .filter(([key]) => (key != 'READABLE' && key != 'WRITABLE') || !(this.get_flags() & GObject.ParamFlags.READWRITE))
            .map(([key, value]) => `GObject.ParamFlags.${key}`)
            .join(" | ");
    }

    get pspecPaths() {
        let [funcPath, typePath] = this.getInfo('type').pspecPaths;
        return { funcPath, typePath, flagsPath: this._flagsPath };
    }

    get pspec() {
        if (!this._pspec) {
            let constructor = imports.gi[this.base.get_namespace()][this.base.get_container().get_name()];
            if (GObject.type_is_a(constructor.$gtype, GObject.TYPE_INTERFACE)) {
                let iface = GObject.type_default_interface_ref(constructor.$gtype);
                this._pspec = GObject.Object.interface_find_property(iface, this.base.get_name());
            } else if (GObject.type_is_a(constructor.$gtype, GObject.TYPE_OBJECT)) {
                this._pspec = GObject.Object.find_property.call(constructor.$gtype, this.base.get_name());
            } else {
                this._pspec = {};
            }
        }

        return this._pspec;
    }
});

export const TypeInfo = registerInfoClass({
    Basename: 'type_info',
}, class TypeInfo extends Info {
    get _debug() {
        if (this.get_tag() == Gir.TypeTag.INTERFACE)
            return "interface: " + Gir.info_type_to_string(this.get_interface().get_type());
        if (this.get_tag() == Gir.TypeTag.ARRAY)
            return "array: " + this.get_array_type() + ", " + Info.newForBase(this.get_param_type(0)).debug;
        return Gir.type_tag_to_string(this.get_tag());
    }

    get string() {
        switch (this.get_tag()) {
            case Gir.TypeTag.VOID:
                return this.is_pointer() ? "pointer" : Gir.type_tag_to_string(this.get_tag());
            case Gir.TypeTag.BOOLEAN:
                return "Boolean";
            case Gir.TypeTag.INT8:
            case Gir.TypeTag.INT16:
            case Gir.TypeTag.INT32:
                return `Number (int)`;
            case Gir.TypeTag.UINT8:
            case Gir.TypeTag.UINT16:
            case Gir.TypeTag.UINT32:
                return `Number (uint)`;
            case Gir.TypeTag.INT64:
                return `Number (int64)`;
            case Gir.TypeTag.UINT64:
                return `Number (uint64)`;
            case Gir.TypeTag.FLOAT:
                return `Number (float)`;
            case Gir.TypeTag.DOUBLE:
                return `Number (double)`;
            case Gir.TypeTag.GTYPE:
                return "GType";
            case Gir.TypeTag.UTF8:
                return `String`;
            case Gir.TypeTag.FILENAME:
            case Gir.TypeTag.UNICHAR:
                return `String (${Gir.type_tag_to_string(this.get_tag())})`;
            case Gir.TypeTag.INTERFACE:
                return `@@${this.get_interface().getPath()}@@`;
            case Gir.TypeTag.ARRAY:
            case Gir.TypeTag.GLIST:
            case Gir.TypeTag.GSLIST:
                return `[${Info.newForBase(this.get_param_type(0)).string}]` +
                    (this.get_array_fixed_size() != -1 ? ` (fixed-size: ${this.get_array_fixed_size()})` : "");
            case Gir.TypeTag.GHASH:
                return `{}`;
            case Gir.TypeTag.ERROR:
                return "GError";
            default:
                throw new Error("Unknow type tag");
        }
    }

    get path() {
        switch (this.get_tag()) {
            case Gir.TypeTag.VOID:
                return "GObject.TYPE_NONE";
            case Gir.TypeTag.BOOLEAN:
                return "GObject.TYPE_BOOLEAN";
            case Gir.TypeTag.INT8:
            case Gir.TypeTag.INT16:
            case Gir.TypeTag.INT32:
                return "GObject.TYPE_INT";
            case Gir.TypeTag.UINT8:
            case Gir.TypeTag.UINT16:
            case Gir.TypeTag.UINT32:
                return "GObject.TYPE_UINT";
            case Gir.TypeTag.INT64:
                return "GObject.TYPE_INT64";
            case Gir.TypeTag.UINT64:
                return "GObject.TYPE_UINT64";
            case Gir.TypeTag.FLOAT:
                return "GObject.TYPE_FLOAT";
            case Gir.TypeTag.DOUBLE:
                return "GObject.TYPE_DOUBLE";
            case Gir.TypeTag.GTYPE:
                return "GObject.TYPE_GTYPE";
            case Gir.TypeTag.UTF8:
            case Gir.TypeTag.FILENAME:
                return "GObject.TYPE_STRING";
            case Gir.TypeTag.ARRAY: {
                switch (this.get_array_type()) {
                    case Gir.ArrayType.C:
                        // XXX: ?
                        return "GObject.TYPE_NONE";
                    case Gir.ArrayType.ARRAY:
                        return "GObject.type_from_name('GArray')";
                    case Gir.ArrayType.PTR_ARRAY:
                        return "GObject.type_from_name('GPtrArray')";
                    case Gir.ArrayType.BYTE_ARRAY:
                        // XXX: "Uint8Array" ?
                        return "GObject.type_from_name('GByteArray')";
                    default:
                        throw new Error("Unknow array type");
                }
            }
            case Gir.TypeTag.INTERFACE:
                return `${this.get_interface().getPath()}.$gtype`;
            case Gir.TypeTag.GLIST:
                return "GObject.TYPE_NONE";
            case Gir.TypeTag.GSLIST:
                return "GObject.TYPE_NONE";
            case Gir.TypeTag.GHASH:
                return "GObject.type_from_name('GHashTable')";
            case Gir.TypeTag.ERROR:
                return "GObject.type_from_name('GError')";
            case Gir.TypeTag.UNICHAR:
                return "GObject.TYPE_UNICHAR";
            default:
                throw new Error("Unknow type tag");
        }
    }

    get pspecPaths() {
        switch (this.get_tag()) {
            case Gir.TypeTag.BOOLEAN:
                return ["GObject.ParamSpec.boolean"];
            case Gir.TypeTag.INT8:
            case Gir.TypeTag.INT16:
            case Gir.TypeTag.INT32:
                return ["GObject.ParamSpec.int"];
            case Gir.TypeTag.UINT8:
            case Gir.TypeTag.UINT16:
            case Gir.TypeTag.UINT32:
                return ["GObject.ParamSpec.uint"];
            case Gir.TypeTag.INT64:
                return ["GObject.ParamSpec.int64"];
            case Gir.TypeTag.UINT64:
                return ["GObject.ParamSpec.uint64"];
            case Gir.TypeTag.FLOAT:
                return ["GObject.ParamSpec.float"];
            case Gir.TypeTag.DOUBLE:
                return ["GObject.ParamSpec.double"];
            case Gir.TypeTag.UTF8:
            case Gir.TypeTag.FILENAME:
            case Gir.TypeTag.UNICHAR:
                return ["GObject.ParamSpec.string"];
            case Gir.TypeTag.ARRAY: {
                let elementTypeTag = Info.newForBase(this.get_param_type(0)).get_tag();
                if (elementTypeTag == Gir.TypeTag.UTF8 || elementTypeTag == Gir.TypeTag.FILENAME)
                    return ['GObject.ParamSpec.boxed', "GObject.type_from_name('GStrv')"];

                return [this._debug];
            }
            case Gir.TypeTag.INTERFACE: {
                let typePath = `${this.get_interface().getPath()}.$gtype`;

                switch (this.get_interface().get_type()) {
                    case Gir.InfoType.BOXED:
                        return ["GObject.ParamSpec.boxed", typePath];
                    case Gir.InfoType.ENUM:
                        return ["GObject.ParamSpec.enum", typePath];
                    case Gir.InfoType.FLAGS:
                        return ["GObject.ParamSpec.flags", typePath];
                    case Gir.InfoType.OBJECT:
                    case Gir.InfoType.INTERFACE:
                        return ["GObject.ParamSpec.object", typePath];
                    default:
                        return [this._debug, typePath];
                }
            }
            default:
                return [this._debug];
        }
    }
});

export const ValueInfo = registerInfoClass({
    Basename: 'value_info',
}, class ValueInfo extends RegisteredTypeInfo {
});
