import Gio from 'gi://Gio';
import Gir from 'gi://GIRepository';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';

export { Gio, Gir, GLib, GObject, Gtk };
