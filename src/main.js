/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported main */

imports.gi.versions.Gtk = '4.0';
const { GLib, Gtk } = imports.gi;

const documents = [{
    title: "Modules",
    spread: [
        'resource:///org/gnome/gjs/modules/core/',
        'resource:///org/gnome/gjs/modules/script/',
    ],
}];

let application = new Gtk.Application();

application.connect('activate', application => {
    let window = new Gtk.ApplicationWindow({
        application,
        defaultWidth: 800, defaultHeight: 450,
        child: new Gtk.Stack(),
    });

    window.set_titlebar(new Gtk.HeaderBar({
        titleWidget: new Gtk.StackSwitcher({ stack: window.child }),
    }));

    window.child.add_titled(new Gtk.Box(), 'evaluation', "Evaluation");
    import('./evaluator/evaluator.js').then(({ default: Evaluator }) => {
        let evaluator = new Evaluator({ hexpand: true });
        window.child.get_child_by_name('evaluation').append(evaluator);
    }).catch(logError);

    documents.forEach(({ title }) => window.child.add_titled(new Gtk.Box(), title.toLowerCase(), title));
    import('./documentor/documentor.js').then(({ default: Documentor }) => {
        documents.forEach(({ title, spread, unspread }) => {
            let documentor = new Documentor();
            window.child.get_child_by_name(title.toLowerCase()).append(documentor);
            let handler = window.child.connect('notify::visible-child-name', stack => {
                if (stack.visibleChildName == title.toLowerCase()) {
                    stack.disconnect(handler);
                    documentor.model.uris = spread;
                    if (unspread)
                        documentor.model.append(...unspread);
                }
            });
        });
    }).catch(logError);

    window.child.add_titled(new Gtk.Box(), 'instrospection', "Introspection");
    import('./introspector/introspector.js').then(({ default: Introspector }) => {
        let introspector = new Introspector();
        window.child.get_child_by_name('instrospection').append(introspector);

        let handler = window.child.connect('notify::visible-child-name', stack => {
            if (stack.visibleChildName == 'instrospection') {
                stack.disconnect(handler);
                introspector.start();
            }
        });
    }).catch(logError);

    window.present();
});

function findUri(relative, datadirs) {
    for (let datadir of datadirs) {
        let filename = GLib.build_filenamev([datadir, relative]);
        if (GLib.file_test(filename, GLib.FileTest.EXISTS))
            return GLib.filename_to_uri(filename, null);
    }

    return null;
}

function main(args, appId, sourceDatadir) {
    let datadirs = sourceDatadir ?
        [sourceDatadir] :
        [GLib.get_user_data_dir()].concat(GLib.get_system_data_dirs()).map(
            dirname => GLib.build_filenamev([dirname, appId])
        );

    let docUri = findUri('gjs/doc', datadirs);
    let examplesUri = findUri('gjs/examples', datadirs);
    if (docUri || examplesUri)
        documents.push({ title: "Doc", spread: docUri ? [docUri] : [], unspread: examplesUri ? [examplesUri] : [] });

    let guideUri = findUri('gjs-guide/docs', datadirs);
    if (guideUri)
        documents.push({ title: "Guide", spread: [guideUri] });

    application.run(args);
}
