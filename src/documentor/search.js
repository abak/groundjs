/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { GLib, GObject, Gtk } from './gi.js';
import Gettext from 'gettext';
import System from 'system';

const SizedLayout = GObject.registerClass({
    Properties: {
        'size': GObject.ParamSpec.int(
            'size', "Size", "The fixed size or -1",
            GObject.ParamFlags.READWRITE, -1, 10000, -1
        ),
    },
}, class extends Gtk.BoxLayout {
    vfunc_measure(widget, orientation, forSize) {
        let [minimum, natural, minimumBaseline, naturalBaseline] =
            super.vfunc_measure(widget, orientation, forSize);

        if (orientation == this.orientation) {
            // Use the first size measured as the default size.
            if (!this._defaultSize)
                this._defaultSize = natural;

            natural = this.size >= 0 ? this.size : this._defaultSize;
            minimum = Math.min(minimum, natural);
        }

        return [minimum, natural, minimumBaseline || -1, naturalBaseline || -1];
    }
});

const TaggedSearchEntry = GObject.registerClass({
    Properties: {
        'show-tag': GObject.ParamSpec.boolean(
            'show-tag', "Show Tag", "Whether the tag is shown",
            GObject.ParamFlags.READWRITE, false
        ),
        'tag': GObject.ParamSpec.string(
            'tag', "Tag", "The tag",
            GObject.ParamFlags.READWRITE, null
        ),
    },
}, class extends Gtk.SearchEntry {
    _init(params) {
        super._init(params);

        this._tagWidget = new Gtk.Label({ cssClasses: ['dim-label'] });
        this.bind_property('tag', this._tagWidget, 'label', GObject.BindingFlags.DEFAULT);
        this.bind_property('show-tag', this._tagWidget, 'visible', GObject.BindingFlags.DEFAULT);
    }

    // No usable dispose/finalize vfuncs with GJS.
    vfunc_realize() {
        this._tagWidget.set_parent(this);
        super.vfunc_realize();
    }

    vfunc_unrealize() {
        super.vfunc_unrealize();
        this._tagWidget.unparent();
    }
});

const occurrencePattern = Gettext.domain('gedit').gettext("%d of %d");
const occurrenceFormat = (a, b) => occurrencePattern.replace('%d', a).replace('%d', b);

const CSS =
`entry { outline-width: 2px; outline-offset: -2px; }
entry:not(:focus-within) { outline-width: 2px; outline-offset: -2px; }
box.linked entry { border-top-right-radius: 0; border-bottom-right-radius: 0; border-right-width: 0; }
`;

export const SearchBox = GObject.registerClass({
    Properties: {
        'buffer': GObject.ParamSpec.object(
            'buffer', "Buffer", "The buffer to search in",
            GObject.ParamFlags.READWRITE, Gtk.TextBuffer.$gtype
        ),
    },
    Signals: {
        // Let the view ensuring the selection visiblity.
        'match-selected': {},
    },
}, class extends Gtk.Box {
    _init(params) {
        super._init(Object.assign({
            marginTop: 4, marginBottom: 4, marginStart: 4, marginEnd: 4,
            cssClasses: ['horizontal', 'linked'],
        }, params));

        this._matchRanges = [];

        this._entry = new TaggedSearchEntry({ layoutManager: new SizedLayout() });
        this._entry.connect('search-changed', this._matchUpdt.bind(this));
        this._entry.connect('previous-match', this._matchPrev.bind(this));
        this._entry.connect('next-match', this._matchNext.bind(this));
        this.append(this._entry);

        let goUpButton = new Gtk.Button({ iconName: 'go-up-symbolic' });
        goUpButton.connect('clicked', this._matchPrev.bind(this));
        this.append(goUpButton);

        let goDownButton = new Gtk.Button({ iconName: 'go-down-symbolic' });
        goDownButton.connect('clicked', this._matchNext.bind(this));
        this.append(goDownButton);

        let keyController = new Gtk.EventControllerKey();
        keyController.connect('key-pressed', (controller_, keyval) => {
            switch (keyval) {
                case Gtk.accelerator_parse('Up')[1]:
                case Gtk.accelerator_parse('KP_Up')[1]:
                    this._matchPrev();
                    return true;
                case Gtk.accelerator_parse('Down')[1]:
                case Gtk.accelerator_parse('KP_Down')[1]:
                    this._matchNext();
                    return true;
                default:
                    return false;
            }
        });
        this._entry.add_controller(keyController);

        let cssProvider = new Gtk.CssProvider();
        cssProvider.load_from_data(CSS);
        Gtk.StyleContext.add_provider_for_display(this.get_display(), cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    }

    _matchNext() {
        if (!this.buffer)
            return;

        let currentIter = this.buffer.get_iter_at_mark(this.buffer.get_insert());
        if (currentIter.is_end())
            currentIter = this.buffer.get_start_iter();

        let range = this._matchRanges.find(([startMark]) => (
            this.buffer.get_iter_at_mark(startMark).compare(currentIter) > 0
        )) ?? this._matchRanges.find(([startMark]) => (
            this.buffer.get_iter_at_mark(startMark).compare(currentIter) < 0
        ));

        if (range)
            this._selectMatchRange(range);
    }

    _matchPrev() {
        if (!this.buffer)
            return;

        let currentIter = this.buffer.get_iter_at_mark(this.buffer.get_insert());
        if (currentIter.is_end())
            currentIter = this.buffer.get_start_iter();

        let range = [...this._matchRanges].reverse().find(([startMark]) => (
            this.buffer.get_iter_at_mark(startMark).compare(currentIter) < 0
        )) ?? [...this._matchRanges].reverse().find(([startMark]) => (
            this.buffer.get_iter_at_mark(startMark).compare(currentIter) > 0
        ));

        if (range)
            this._selectMatchRange(range);
    }

    _matchUpdt() {
        if (!this.buffer)
            return;

        if (this._matchTag) {
            this.buffer.remove_tag(this._matchTag, ...this.buffer.get_bounds());
            this._matchTag.set_priority(this.buffer.tagTable.get_size() - 1);
        } else {
            this.buffer.tagTable.add(this._matchTag = new Gtk.TextTag({
                background: 'chartreuse3',
                foreground: 'white',
            }));
        }

        this._matchRanges.forEach(([startMark, endMark]) => {
            this.buffer.delete_mark(startMark);
            this.buffer.delete_mark(endMark);
        });
        this._matchRanges = [];

        let string = this._entry.text;
        if (!string) {
            this._entry.remove_css_class('error');

            // Avoid single-character selection remainder.
            if (this.buffer.get_has_selection()) {
                let insertIter = this.buffer.get_iter_at_mark(this.buffer.get_insert());
                this.buffer.move_mark(this.buffer.get_selection_bound(), insertIter);
            }

            this._entry.showTag = false;

            return;
        }

        let iter = this.buffer.get_start_iter();
        while (!iter.is_end()) {
            let [success, matchStartIter, matchEndIter] =
                iter.forward_search(string, Gtk.TextSearchFlags.TEXT_ONLY | Gtk.TextSearchFlags.CASE_INSENSITIVE, null);

            if (success) {
                this.buffer.apply_tag(this._matchTag, matchStartIter, matchEndIter);

                this._matchRanges.push([
                    this.buffer.create_mark(null, matchStartIter, false),
                    this.buffer.create_mark(null, matchEndIter, false)
                ]);

                iter = matchEndIter;
            } else {
                break;
            }
        }

        if (this._matchRanges.length) {
            this._entry.remove_css_class('error');

            let currentIter = this.buffer.get_iter_at_mark(this.buffer.get_insert());
            if (currentIter.is_end())
                currentIter = this.buffer.get_start_iter();

            let range = this._matchRanges.find(([startMark, endMark]) => (
                this.buffer.get_iter_at_mark(startMark).compare(currentIter) >= 0
            )) ?? this._matchRanges[this._matchRanges.length - 1];

            this._selectMatchRange(range);
        } else {
            this._entry.add_css_class('error');
            this._entry.showTag = false;
        }

        System.gc();
    }

    _selectMatchRange(range) {
        let [startIter, endIter] = range.map(mark => this.buffer.get_iter_at_mark(mark));

        this.buffer.select_range(startIter, endIter);
        this.emit('match-selected');

        this._entry.tag = occurrenceFormat(this._matchRanges.indexOf(range) + 1, this._matchRanges.length);
        this._entry.showTag = true;
    }

    vfunc_grab_focus() {
        this._entry.grab_focus();
    }

    get buffer() {
        return this._buffer ?? null;
    }

    set buffer(buffer) {
        if (this.buffer == buffer)
            return;

        if (this.buffer)
            this.buffer.disconnect(this._bufferChangedHandler);

        this._buffer = buffer;
        this.notify('buffer');

        this._bufferChangedHandler = this.buffer?.connect('changed', () => {
            if (!this.get_mapped())
                return;

            if (this._bufferChangedTimeout)
                GLib.source_remove(this._bufferChangedTimeout);

            this._bufferChangedTimeout = GLib.timeout_add(GLib.PRIORITY_LOW, 75, () => {
                this._matchUpdt();

                delete this._bufferChangedTimeout;
                return GLib.SOURCE_REMOVE;
            });
        });
    }

    getEntry() {
        return this._entry;
    }
});

// Like GtkSearchBar, but it is meant for taking place in an overlay.
export const SearchSlider = GObject.registerClass({
    Properties: {
        'child': GObject.ParamSpec.object(
            'child', "Child", "The child widget",
            GObject.ParamFlags.READWRITE, Gtk.Widget.$gtype
        ),
        'key-capture-widget': GObject.ParamSpec.object(
            'key-capture-widget', "Key Capture Widget", "The widget to capture 'Ctrl + F' and 'Escape' keys on",
            GObject.ParamFlags.READWRITE, Gtk.Widget.$gtype
        ),
        'search-mode-enabled': GObject.ParamSpec.boolean(
            'search-mode-enabled', "Search Mode Enabled", "Whether the search mode is on and the search slider shown",
            GObject.ParamFlags.READWRITE, false
        ),
        'show-close-button': GObject.ParamSpec.boolean(
            'show-close-button', "Show Close Button", "Whether to show the close button",
            GObject.ParamFlags.READWRITE, false
        ),
    },
}, class extends Gtk.Widget {
    _init(params) {
        this._revealer = new Gtk.Revealer({
            marginTop: 6, marginEnd: 12,
            cssClasses: ['background'],
            child: new Gtk.Frame({
                child: new Gtk.Box(),
            }),
        });

        this._closeButton = new Gtk.Button({
            cssClasses: ['close', 'flat'],
            iconName: 'window-close-symbolic',
            visible: false,
        });
        this._closeButton.connect('clicked', () => (this.searchModeEnabled = false));
        this._revealer.child.child.append(this._closeButton);

        this._shortcutController = new Gtk.ShortcutController({
            propagationPhase: Gtk.PropagationPhase.CAPTURE,
        });
        this._shortcutController.add_shortcut(new Gtk.Shortcut({
            trigger: Gtk.ShortcutTrigger.parse_string('<Control>F'),
            action: Gtk.CallbackAction.new(() => {
                let [hasSelection, startIter, endIter] = this.child?.buffer?.get_selection_bounds() ?? [];

                if (hasSelection) {
                    this.child.getEntry().set_text(this.child.buffer.get_text(startIter, endIter, true));
                    this.child.getEntry().set_position(-1);
                    this.searchModeEnabled = true;
                } else {
                    this.searchModeEnabled = !this.searchModeEnabled;
                }
            }),
        }));
        this._shortcutController.add_shortcut(new Gtk.Shortcut({
            trigger: Gtk.ShortcutTrigger.parse_string('Escape'),
            action: Gtk.CallbackAction.new(() => {
                this.searchModeEnabled = false;
            }),
        }));

        super._init(params);
    }

    // No usable dispose/finalize vfuncs with GJS.
    vfunc_realize() {
        this._revealer.set_parent(this);
        super.vfunc_realize();
    }

    vfunc_unrealize() {
        super.vfunc_unrealize();
        this._revealer.unparent();
    }

    get child() {
        return [...this._closeButton.parent].find(widget => widget != this._closeButton) ?? null;
    }

    set child(child) {
        if (this.child == child)
            return;

        if (this.child)
            this._closeButton.parent.remove(this.child);
        if (child) {
            this._closeButton.parent.prepend(child);
            let { marginTop, marginBottom, marginStart, marginEnd } = child;
            this._closeButton.set({ marginTop, marginBottom, marginStart, marginEnd });
        }

        this.notify('child');
    }

    get keyCaptureWidget() {
        return this._shortcutController.widget ?? null;
    }

    set keyCaptureWidget(keyCaptureWidget) {
        if (this._keyCaptureWidget == keyCaptureWidget)
            return;

        if (this.keyCaptureWidget)
            this.keyCaptureWidget.remove_controller(this._shortcutController);

        this.notify('key-capture-widget');

        if (keyCaptureWidget)
            keyCaptureWidget.add_controller(this._shortcutController);
    }

    get searchModeEnabled() {
        return this._revealer.revealChild;
    }

    set searchModeEnabled(searchModeEnabled) {
        if (this.searchModeEnabled == searchModeEnabled)
            return;

        this._revealer.revealChild = searchModeEnabled;
        this.notify('search-mode-enabled');

        if (searchModeEnabled) {
            this.child?.grab_focus();
        } else {
            this.child?.getEntry().set_text('');
        }
    }

    get showCloseButton() {
        return this._closeButton.visible;
    }

    set showCloseButton(showCloseButton) {
        if (this.showCloseButton == showCloseButton)
            return;

        this._closeButton.visible = showCloseButton;
        this.notify('show-close-button');
    }
});
SearchSlider.set_layout_manager_type(Gtk.BinLayout.$gtype);
