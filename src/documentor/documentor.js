/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { Gio, GObject, Gtk } from './gi.js';
import { SearchBox, SearchSlider } from './search.js';
const ByteArray = imports.byteArray;

const filterFunc = function(info) {
    if (info.get_is_hidden())
        return false;

    if (info.get_file_type() == Gio.FileType.DIRECTORY)
        return true;

    return !!(info.get_content_type().match(/(javascript|markdown|builder)/));
};

const sortFunc = function(info1, info2) {
    let name1 = info1.get_display_name().replace(/^_/, '');
    let name2 = info2.get_display_name().replace(/^_/, '');

    return name1.localeCompare(name2);
};

const FileList = GObject.registerClass({
    Implements: [Gio.ListModel],
    Properties: {
        'uris': GObject.ParamSpec.boxed(
            'uris', "Uris", "The location uris",
            // TODO: Report to GJS: GObject.TYPE_STRV is missing.
            GObject.ParamFlags.READWRITE, GObject.type_from_name('GStrv')
        ),
    },
}, class FileList extends GObject.Object {
    vfunc_get_item_type() {
        return Gio.FileInfo.$gtype;
    }

    vfunc_get_item(position) {
        return this._items?.[position] ?? null;
    }

    vfunc_get_n_items() {
        return this._items?.length ?? 0;
    }

    vfunc_notify(pspec) {
        if (pspec.get_name() != 'uris')
            return;

        let items = [];
        this.uris.forEach(uri => {
            try {
                let directory = Gio.File.new_for_uri(uri);
                let info, enumerator = directory.enumerate_children('standard::*', Gio.FileQueryInfoFlags.NONE, null);
                while ((info = enumerator.next_file(null))) {
                    let file = enumerator.get_child(info);
                    info.set_attribute_object('standard::file', file);
                    items.push(info);
                }
                enumerator.close(null);
            } catch(e) {
                log(e.message);
            }
        });

        let removed = this._items?.length ?? 0;
        this._items = items.filter(filterFunc).sort(sortFunc);
        this.items_changed(0, removed, this._items.length);
    }

    // FIXME: It is not generic.
    append(...uris) {
        let items = [];
        uris.forEach(uri => {
            try {
                let file = Gio.File.new_for_uri(uri);
                let info = file.query_info('standard::*', Gio.FileQueryInfoFlags.NONE, null);
                info.set_attribute_object('standard::file', file);
                info.set_display_name(info.get_display_name()[0].toUpperCase() + info.get_display_name().slice(1));
                if (filterFunc(info))
                    items.push(info);
            } catch(e) {
                log(e.message);
            }
        });

        this._items.push(...items);
        this.items_changed(this._items.length - items.length, 0, items.length);
    }
});

let hasHighlightProgram = true;

export default GObject.registerClass({
    Properties: {
        'model': GObject.ParamSpec.object(
            'model', "Model", "The model",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, FileList.$gtype
        ),
    },
}, class FileContentsView extends Gtk.Box {
    _init(params) {
        super._init(Object.assign({
            model: new FileList(),
        }, params));

        let cssProvider = new Gtk.CssProvider();
        cssProvider.load_from_data('textview.monospace { font-size: smaller; }');
        Gtk.StyleContext.add_provider_for_display(this.get_display(), cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        let treeListModel = Gtk.TreeListModel.new(
            this.model,
            false, false,
            item => {
                if (item.get_file_type() != Gio.FileType.DIRECTORY)
                    return null;

                return new FileList({ uris: [item.get_attribute_object('standard::file').get_uri()] });
            },
        );

        let factory = new Gtk.SignalListItemFactory();
        factory.connect('setup', (factory_, listItem) => {
            listItem.child = new Gtk.TreeExpander({
                child: new Gtk.Label({
                    xalign: 0,
                    ellipsize: 3, // Pango.EllipsizeMode.END
                    widthChars: 15,
                }),
            });
        });
        factory.connect('bind', (factory_, listItem) => {
            let listRow = listItem.item;
            let expander = listItem.child;

            expander.child.label = listRow.item.get_display_name();
            if (listRow.item.get_display_name().length > expander.child.widthChars + 2)
                expander.child.tooltipText = listRow.item.get_display_name();
            expander.listRow = listRow;
        });

        let listView = new Gtk.ListView({
            factory,
            model: new Gtk.SingleSelection({
                model: treeListModel,
            }),
            cssClasses: ['view', 'navigation-sidebar'],
        });

        this.append(new Gtk.ScrolledWindow({
            child: listView,
            hscrollbarPolicy: Gtk.PolicyType.NEVER,
        }));

        this.append(new Gtk.Separator({
            orientation: Gtk.Orientation.VERTICAL,
        }));

        this._buffer = new Gtk.TextBuffer();

        let textView = new Gtk.TextView({
            buffer: this._buffer,
            monospace: true, editable: false,
            leftMargin: 12, rightMargin: 12,
            topMargin: 12, bottomMargin: 12,
            wrapMode: Gtk.WrapMode.WORD_CHAR,
        });

        let searchBox = new SearchBox({
            buffer: this._buffer,
        });
        searchBox.connect('match-selected', () => {
            let [, insertIter] = this._buffer.get_selection_bounds();
            if (textView.get_iter_location(insertIter).y < textView.get_visible_rect().y)
                textView.scroll_mark_onscreen(this._buffer.get_insert());
            else
                textView.scroll_mark_onscreen(this._buffer.get_selection_bound());
        });

        let searchSlider = new SearchSlider({
            halign: Gtk.Align.END,
            valign: Gtk.Align.START,
            child: searchBox,
            keyCaptureWidget: this,
            showCloseButton: true,
        });

        let overlay = new Gtk.Overlay({
            child: new Gtk.ScrolledWindow({
                child: textView,
                hexpand: true,
            }),
        });
        overlay.add_overlay(searchSlider);
        this.append(overlay);

        listView.model.connect('notify::selected-item', selection => {
            let listRow = selection.selectedItem;
            if (!listRow || listRow.expandable) {
                this._buffer.text = '';
                return;
            }

            try {
                this._showHighlight(listRow.item.get_attribute_object('standard::file'));
            } catch(e) {
                logError(e);
            }
        });

        listView.connect('activate', (listView, position) => {
            let info = listView.model.get_item(position).item;
            let file = info.get_attribute_object('standard::file');

            if (file.get_path() && info.get_file_type() == Gio.FileType.REGULAR)
                Gio.AppInfo.launch_default_for_uri(file.get_uri(), null);
        });
    }

    _showHighlight(file) {
        this._buffer.text = '';
        let bytes = file.load_bytes(null)[0];

        if (!hasHighlightProgram) {
            this._showText(bytes);
            return;
        }

        let color = this.get_style_context().get_color();
        let dark = (color.red + color.green + color.blue) > 3 / 2;

        try {
            Gio.Subprocess.new(
                [
                    'highlight', '--fragment', '--out-format=pango',
                    `--style=${dark ? 'edit-vim-dark' : 'edit-kwrite'}`,
                    `--syntax-by-name=${file.get_basename().replace(/\.ui$/, '.xml')}`,
                ],
                Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE
            ).communicate_async(bytes, null, (subprocess, result) => {
                try {
                    let [success_, stdout, stderr] = subprocess.communicate_finish(result);
                    if (subprocess.get_exit_status() != 0 || !stdout) {
                        if (stderr)
                            log(ByteArray.toString(stderr.get_data()));

                        return;
                    }

                    let markup = ByteArray.toString(stdout.get_data());
                    // Remove the global font attributes.
                    markup = markup.slice(markup.indexOf('>') + 1, markup.lastIndexOf('<')).trim();

                    this._buffer.insert_markup(this._buffer.get_start_iter(), markup, -1);
                } catch(e) {
                    this._showText(bytes);
                }
            });
        } catch(e) {
            log("For syntax highlighting, install the “highlight” program");
            hasHighlightProgram = false;
            this._showText(bytes);
        }
    }

    _showText(bytes) {
        try {
            this._buffer.text = ByteArray.toString(ByteArray.fromGBytes(bytes));
        } catch(e) {}
    }
});
